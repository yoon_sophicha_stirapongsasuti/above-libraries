#include "Arduino.h"
#include "strap.h"

Strap::Strap(int pin){
  pinMode(pin, INPUT_PULLUP);
}

bool Strap::is_connect(void){
  strap_status = digitalRead(16);
  connection_status = true;

  if(!strap_status){
    Serial.println("Strap Disconnected");
	connection_status = false;
  }
  else{
    Serial.println("Strap Connected");
  }
  delay(3000);

  return connection_status;
}

