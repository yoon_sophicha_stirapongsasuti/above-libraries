#include "led_indicator.h"

 Led_indicator::Led_indicator(){
	 
 }

void Led_indicator::led_init(int led_g, int led_r, int led_b){
  lred = led_r;
  lgreen = led_g;
  lblue = led_b;
  
  pinMode(lgreen, OUTPUT);     // Initialize the LED_GREEN pin as an output
  pinMode(lred, OUTPUT);       // Initialize the LED_RED pin as an output
  pinMode(lblue, OUTPUT);      // Initialize the LED_BLUE pin as an output
}

void Led_indicator::setColor(int red, int green, int blue)
{
digitalWrite(lred, red);
digitalWrite(lgreen, green);
digitalWrite(lblue, blue);
}

void Led_indicator::led_control(char led_control){
  switch(led_control)
  {
    case 'o':          //off
      setColor(HIGH, HIGH, HIGH);
      break;
    
    case 'r':         //red
      setColor(LOW, HIGH, HIGH);
      break;
    
    case 'g':         //green
      setColor(HIGH, LOW, HIGH);   
      break;
    
    case 'b':         //blue
      setColor(HIGH, HIGH, LOW);
      break;
    
    case 'y':         //yellow
      setColor(LOW, LOW, HIGH); 
      break;
    
    case 'v':         //violet
      setColor(LOW, HIGH, LOW);
      break;
    
    case 'a':         //aqua
      setColor(HIGH, LOW, LOW);
      break;

    case 'w':         //white
      setColor(LOW, LOW, LOW);
      break;
    
    default:
      break;
  }
}



