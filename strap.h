#ifndef strap_h
#define strap_h

#include "Arduino.h"

class Strap{
  public:

Strap(int pin);
bool is_connect(void);

  private:
int strap_status;
bool connection_status;
};

#endif
