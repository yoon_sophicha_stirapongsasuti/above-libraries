#include "wifi_client.h"

WiFi_client::WiFi_client(){
  
}

void WiFi_client::wifi_init(char* ssid, char* password){
  // We start by connecting to a WiFi network
    WiFiMulti.addAP(ssid, password);

    Serial.println();
    Serial.println();
    Serial.print("Wait for WiFi... ");

    while(WiFiMulti.run() != WL_CONNECTED) {
        Serial.print(".");
        delay(500);
    }

	is_connect = true;
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

    delay(500);
}

bool WiFi_client::start_wifi_connection(uint16_t port, char* host){
//    const uint16_t port = 54816;
//    const char * host = "10.0.3.20"; // ip or dns
    bool conn_status = false;
    Serial.println("start connection");

    if (!client.connect(host, port)) {
        Serial.println("connection failed");
        Serial.println("wait 5 sec...");
        delay(5000);
    }
    else {
    Serial.print("connecting to ");
    Serial.println(host);
    conn_status = true;
    }
     return conn_status;
}

