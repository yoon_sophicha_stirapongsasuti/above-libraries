#ifndef wifi_client_h
#define wifi_client_h

#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

class WiFi_client{
  public:

WiFi_client();
bool is_connect = false;
WiFiClient client;
void wifi_init(char* ssid, char* password);
bool start_wifi_connection(uint16_t port, char* host);

  private:
ESP8266WiFiMulti WiFiMulti;
};

#endif

