#ifndef led_indicator_h
#define led_indicator_h

#include "Arduino.h"

class Led_indicator{
  public:

Led_indicator();
void led_init(int led_g, int led_r, int led_b);
void led_control(char led_control);

  private:
void setColor(int red, int green, int blue);  
int lred, lgreen, lblue;
};

#endif



